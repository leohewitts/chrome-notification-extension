/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
export async function setChromeBadge(text: string, color = '#1874e8') {
    await chrome.action.setBadgeBackgroundColor({ color })

    await chrome.action.setBadgeText({ text })
}

export async function openNewTab(url: string) {
    await chrome.tabs.create({
        url
    })
}

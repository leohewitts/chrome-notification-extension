export function toStrWithSpace(str: string): string {
    return str.replace(/([A-Z])/g, ' $1').trim()
}

export function toStrWithoutSpace(str: string): string {
    return (str = str.replace(/\s*/g, ''))
}

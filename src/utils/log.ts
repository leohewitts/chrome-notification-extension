// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function log(data: any, desc?: string): void {
    if (desc) {
        console.log(desc)
    }

    if (typeof data === 'object') {
        console.dir(data)
    } else {
        console.log(data)
    }

    console.log('<=========>')
}

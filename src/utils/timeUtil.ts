/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
export function getTimeObj(dateSign: number) {
    const dateObj = new Date(dateSign)
    const dateStr = dateObj.toString()

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [_, month, day, year, time] = dateStr.split(' ')

    const utcOffset = -(dateObj.getTimezoneOffset() / 60)

    return {
        month,
        day,
        year,
        time,
        utcOffset
    }
}

export function getTimeStr(dateSign: number): string {
    if (!dateSign) {
        return ''
    }

    const { month, day, year, time, utcOffset } = getTimeObj(dateSign)

    const utcOffsetStr = utcOffset > 0 ? `(UTC+${utcOffset})` : `(UTC${utcOffset})`

    const timeStr = `${month} ${day},${year} ${time}${utcOffsetStr}`

    return timeStr
}

/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {
    ChromeStorageKey,
    Message,
    NetworkType,
    NetworkTypeItem,
    ShowPositionHashArr,
    ShowCreateByArr,
    ShowBidderArr,
    MsgLog
} from '../types/base'

import { setChromeBadge } from '../utils/chromeHelper'
import { getTimeObj } from '../utils/timeUtil'
import { log } from '../utils/log'

import { getCurrentNetType } from './networkAction'
import { getTokenList } from './tokenList'

export async function addMsg(msg: MsgLog) {
    log(msg, 'receive msg to add')
    const parsedMsg = await parseMsgToShow(msg)
    log(parsedMsg, 'msg after parsed')

    const msgs = await getMsgsFromStorage()

    msgs.push(parsedMsg)
    await setMsgsToStorage(msgs)

    await updateMsgNum()
    await updateLastUpdateTime()
}

export async function updateMsgNum() {
    const msgs = await getMsgsFromStorage()
    log(msgs, 'before updateMsgNum, msg in storage is')

    if (!msgs) {
        return
    }

    const unreadNum = msgs.filter((m: Message) => !m.isReaded).length

    const actionNum = unreadNum > 99 ? '99+' : `${unreadNum || ''}`

    if (unreadNum) {
        await setChromeBadge(actionNum)
    } else {
        await setChromeBadge('')
    }
}

export async function toggleMsgType(newType: NetworkTypeItem, oldType?: NetworkTypeItem) {
    const oldTypeValue = oldType || (newType === NetworkType.Testnet_Goerli ? NetworkType.Mainnet : NetworkType.Testnet_Goerli)

    const msgs = await getMsgsFromStorage()
    log(msgs, 'after toggle msg type, msgs is')

    const inActiveMsgsStorage = await chrome.storage.local.get(ChromeStorageKey.InActiveMsgs)
    const inActiveMsgs = inActiveMsgsStorage[ChromeStorageKey.InActiveMsgs] || {}
    log(inActiveMsgs, 'inActiveMsgs is')

    inActiveMsgs[oldTypeValue] = JSON.stringify(msgs)
    await chrome.storage.local.set({
        [ChromeStorageKey.InActiveMsgs]: inActiveMsgs
    })

    await setMsgsToStorage(JSON.parse(inActiveMsgs[newType] || '[]'))
}

export async function parseMsgToShow(msgLog: MsgLog): Promise<Message> {
    const msg = {
        ...msgLog,
        ...msgLog.origin,
        ...msgLog.log
    } as Message

    const logoURI = await getLogoURI(msg)
    const poolId = await getPoolId(msg)
    const { hash, hashStr } = await getHashStr(msg)
    const receiveTimeObj = getTimeAndDateStr(msg.receiveTime)

    const parseMsg = {
        ...msg,

        positionAddr: msg?.args?.positionAddr,
        proposalId: (msg?.args?.proposalId || '').toString(),

        id: Math.floor(Math.random() * 100000),
        eventName: msg.name,
        name: msg.type,

        hash,
        hashStr,
        logoURI,
        poolId,

        isChecked: false,
        isReaded: false,

        readedTime: null,
        receiveTime: new Date().getTime(),
        receiveDateStr: receiveTimeObj.date,
        receiveTimeStr: receiveTimeObj.time
    }

    return parseMsg
}

export async function updateLastUpdateTime() {
    const time = new Date().getTime()
    await chrome.storage.local.set({
        [ChromeStorageKey.LastUpdateTime]: time
    })
}

async function getHashStr(msg: Message) {
    let hash = ''

    const msgType = msg.type as any
    if (ShowPositionHashArr.has(msgType)) {
        hash = msg?.args?.positionAddr || ''
    }
    if (ShowCreateByArr.has(msgType)) {
        hash = await getProposer(msg)
    }
    if (ShowBidderArr.has(msgType)) {
        hash = msg?.args?.ruler
    }

    const start = hash.substring(0, 6)
    const end = hash.substring(hash.length - 4, hash.length)
    const hashStr = start && end ? `${start}...${end}` : ''

    return { hash, hashStr }
}

function getTimeAndDateStr(dateSign?: number) {
    const timeObj = getTimeObj(dateSign || new Date().getTime())

    const nowYear = new Date().getFullYear().toString()
    const date = nowYear === timeObj.year ? `${timeObj.month} ${timeObj.day}` : `${timeObj.year} ${timeObj.month} ${timeObj.day}`
    const time = timeObj.time

    return { date, time }
}

async function getPoolId(msg: Message) {
    let poolId: string
    if (msg.args && msg.args.poolId) {
        poolId = msg.args.poolId.toString()
    } else if (msg.args && msg.args.positionAddr) {
        const positionInfo = await globalThis.Thigh.module.position.getInfoByAddress(msg.args.positionAddr)
        poolId = positionInfo?.poolId?.toString()
    }
    return poolId
}

async function getLogoURI(msg: Message) {
    const currentNetType = await getCurrentNetType()
    const tokens = await getTokenList(currentNetType)

    let tokenAddress: string
    const poolId = await getPoolId(msg)
    const proposalId = msg?.args?.proposalId
    if (poolId) {
        tokenAddress = await getTokenAddressByPooId(poolId)
    } else if (proposalId) {
        tokenAddress = await getTokenAddressByProposalId(proposalId)
    }

    const tokenItem = tokens.find((t: { address: any }) => t.address === tokenAddress)
    return tokenItem?.logoURI || ''
}

async function getTokenAddressByPooId(poolId: string) {
    if (!poolId) return ''

    const poolInfo = await globalThis.Thigh.module.pool.getPoolInfo(poolId)
    const stakedToken = poolInfo.stakedToken
    const tokenAddress = stakedToken.address

    return tokenAddress
}

async function getTokenAddressByProposalId(proposalId: string) {
    if (!proposalId) return ''
    
    const proposalInfo = await globalThis.Thigh.module.committee.poolMetersMap(proposalId)
    const tokenAddress = proposalInfo.tokenContract
    
    return tokenAddress
}

async function getProposer(msg: Message) {
    let proposer: string
    if (msg?.args?.proposer) {
        proposer = msg.args.proposer
    } else if (msg?.args?.proposalId) {
        const proposalId = msg.args.proposalId.toNumber()
        const proposalInfo = await globalThis.Thigh.module.committee.getProposalInfoById(proposalId)
        proposer = proposalInfo.proposer
    }

    return proposer || ''
}

export async function getMsgsFromStorage() {
    const msgsStorage = await chrome.storage.local.get(ChromeStorageKey.Msgs)
    const msgs = JSON.parse(msgsStorage.msgs || '[]') as Array<Message>

    return msgs
}

export async function setMsgsToStorage(msgs: Array<Message>) {
    await chrome.storage.local.set({ [ChromeStorageKey.Msgs]: JSON.stringify(msgs) })
}

/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import { ChromeStorageKey } from '../types/base'
import { Setting, DefaultSettings } from '../types/settingType'

import { listenerBidingTanto } from './msgEventListener'
import { getCurrentNetType, refreshNet } from './networkAction'
import manifest from '../manifest.json'

export async function initDefaultSetting() {
    const settingVal = await getLocalSetting()

    const version = manifest.version
    const isRightVersion = settingVal.version && settingVal.version === version

    if (!settingVal || !isRightVersion) {
        await setSettingToStorage(DefaultSettings)
    }
}

export async function handleSettingChange(settingChange: { newValue: string; oldValue: string }) {
    const change = {
        ...settingChange,
        oldValue: (settingChange.oldValue ? JSON.parse(settingChange.oldValue) : {}) as Setting,
        newValue: (settingChange.newValue ? JSON.parse(settingChange.newValue) : DefaultSettings) as Setting
    }

    handleBiddingTantosChange(change)
    await handleRpcChange(change)
}

function handleBiddingTantosChange(settingChange: { newValue: Setting; oldValue: Setting }) {
    const { newValue, oldValue } = settingChange

    const oldBiddingTantos = oldValue?.biddingTantos || []
    const newBiddingTantos = newValue?.biddingTantos || []
    const biddingTantoOpen = newValue?.biddingTantoOpen || false

    if (!biddingTantoOpen) return

    for (const bdt of newBiddingTantos) {
        if (oldBiddingTantos.every(b => b.value !== bdt.value)) {
            globalThis.Thigh.eventListener.on('auctionHall', 'BidTanto', listenerBidingTanto(bdt.value), bdt.value)
        }
    }
}

async function handleRpcChange(settingChange: { newValue: Setting; oldValue: Setting }) {
    const { newValue, oldValue } = settingChange

    const oldRpcConfig = oldValue?.rpc || {}
    const newRpcConfig = newValue.rpc
    const networkType = await getCurrentNetType()
    if (oldRpcConfig[networkType] !== newRpcConfig[networkType]) {
        await refreshNet()
    }
}

export async function getLocalSetting() {
    const settingStorage = await chrome.storage.local.get(ChromeStorageKey.Settings)
    if (settingStorage && settingStorage[ChromeStorageKey.Settings]) {
        const settings = JSON.parse(settingStorage[ChromeStorageKey.Settings]) as Setting
        return settings
    }

    return null
}

export async function getSettingFromStorage() {
    const settings = await getLocalSetting()
    return settings || DefaultSettings
}

export async function setSettingToStorage(setting: Setting) {
    setting.version = manifest.version

    await chrome.storage.local.set({ [ChromeStorageKey.Settings]: JSON.stringify(setting) })
}

/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { DefaultNetType } from '../types/settingType'
import { ChromeStorageKey, NetworkType, NetworkTypeItem } from '../types/base'
import { getSettingFromStorage } from './settingAction'
import { updateBlockNum, getBlockNumber } from './blockNumAction'
import { log } from '../utils/log'
import { setBadgeError } from './chromeAction'
import { listenEventMsg } from './msgEventListener'

export async function initDefaultNetType() {
    const netStorage = await chrome.storage.local.get(ChromeStorageKey.NetworkType)

    if (!netStorage[ChromeStorageKey.NetworkType]) {
        await chrome.storage.local.set({
            [ChromeStorageKey.NetworkType]: DefaultNetType
        })
    }
}

export async function refreshNet() {
    try {
        const netParams = await getNetParams()
        log(netParams, 'update thigh with params')

        globalThis.Thigh.config.update(netParams)

        await listenEventMsg()
        await updateBlockNum('')
        await getBlockNumber()
    } catch (error) {
        log(error, 'update tigher error')
        await setBadgeError()
    }
}

export async function getNetParams() {
    const networkType = await getCurrentNetType()
    const settings = await getSettingFromStorage()
    const rpcConfig = settings.rpc

    const thigh_rpc = rpcConfig[networkType]
    if (networkType === NetworkType.Mainnet) {
        return {
            thigh_rpc,
            thigh_chainId: '0x01'
        }
    } else {
        return {
            thigh_rpc,
            thigh_chainId: '0x05'
        }
    }
}

export async function getCurrentNetType() {
    const netStorage = await chrome.storage.local.get(ChromeStorageKey.NetworkType)
    const networkType = (netStorage.networkType as NetworkTypeItem) || DefaultNetType

    return networkType
}

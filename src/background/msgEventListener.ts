/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unsafe-call */

import { log } from '../utils/log'
import { EventItem, EventType, MsgLog, ProposalStatusMap } from '../types/base'
import { BdTantoObj } from '../types/settingType'
import { addMsg } from './msgAction'
import { getSettingFromStorage } from './settingAction'
import { setBadgeError } from './chromeAction'

export async function listenEventMsg() {
    try {
        globalThis.Thigh.eventListener.on('tradingHub', 'PositionOpened', listenSingleEvent(EventType.Position_Opened))
        globalThis.Thigh.eventListener.on('tradingHub', 'PositionClosing', listenSingleEvent(EventType.New_Liquidation))
        globalThis.Thigh.eventListener.on('committee', 'PoolProposalCreated', listenSingleEvent(EventType.New_Proposal))

        globalThis.Thigh.eventListener.on('tradingHub', 'PositionOverdrawn', listenPositionOverDrawOrAborted)
        globalThis.Thigh.eventListener.on('committee', 'ProposalStatusChanged', listenProposalPassOrFailed)

        const settings = await getSettingFromStorage()
        const biddingTantos: Array<BdTantoObj> = settings.biddingTantos || []
        for (const bdt of biddingTantos) {
            globalThis.Thigh.eventListener.on('auctionHall', 'BidTanto', listenerBidingTanto(bdt.value), bdt.value)
        }
    } catch (error) {
        log(error, 'init listener error')
        await setBadgeError()
    }
}

function listenSingleEvent(eventName: EventItem) {
    const listener = async (msg: MsgLog) => {
        log(msg, `${eventName} received msg, is:`)
        const settings = await getSettingFromStorage()
        const events = settings?.events || null
        log(events, 'setting events:')

        const canAdd = events && events.indexOf(eventName) !== -1
        msg.log.type = eventName

        log(canAdd, 'can add:')
        if (canAdd) {
            void addMsg(msg)
        }
    }

    return listener
}

function listenPositionOverDrawOrAborted(msg: MsgLog) {
    log(msg, 'position over draw or aborted received msg, is:')
    void getSettingFromStorage().then(async settings => {
        const events = settings?.events || null
        log(events, 'setting events:')

        const isAborted = await isLiquidationAborted(msg)
        
        let canAdd = false
        if (isAborted) {
            canAdd = events &&  events.indexOf(EventType.Liquidation_Aborted) !== -1
            msg.log.type = EventType.Liquidation_Aborted
        } else {
            canAdd = events && events.indexOf(EventType.Position_Overdrawn) !== -1
            msg.log.type = EventType.Position_Overdrawn
        }

        log(canAdd, 'can add:')
        if (canAdd) {
            void addMsg(msg)
        }
    })
}

function listenProposalPassOrFailed(msg: MsgLog) {
    log(msg, 'proposal pass or failed received msg, is:')
    void getSettingFromStorage().then(settings => {
        const events = settings?.events || null
        log(events, 'setting events:')

        const ps = msg?.log?.args?.ps?.toNumber()
        
        // only add passed or failed proposalStatusChange msg
        let canAdd = false
        if (ps === ProposalStatusMap.Passed) {
            canAdd = events && (events.indexOf(EventType.Proposal_Passed) !== -1)
            msg.log.type = EventType.Proposal_Passed
        }
        if (ps === ProposalStatusMap.Failed) {
            canAdd = events && (events.indexOf(EventType.Proposal_Failed) !== -1)
            msg.log.type = EventType.Proposal_Failed
        }

        log(`can add: ${canAdd}`)
        if (canAdd) {
            void addMsg(msg)
        }
    })
}

export function listenerBidingTanto(hash: string) {
    const listener = (msg: MsgLog) => {
        log(msg, 'biding tanto received msg, is:')
        void getSettingFromStorage().then(settings => {
            const biddingTantoOpen = settings.biddingTantoOpen || false
            const biddingTantos: Array<BdTantoObj> = settings.biddingTantos || []
            log(biddingTantos, 'setting BidTantos:')

            const canAdd = biddingTantoOpen && biddingTantos.some((bdt: BdTantoObj) => bdt.value === hash)
            msg.log.type = EventType.Tanto_Bidding

            log(canAdd, 'can add:')
            if (canAdd) {
                void addMsg(msg)
            }
        })
    }

    return listener
}

async function isLiquidationAborted(msg: MsgLog) {
    const blockNum: number = msg.origin.blockNumber

    const positionHash = msg?.log?.args?.positionAddr
    const positionBlock = await globalThis.Thigh.module.position.getPositionBlocks(positionHash)
    const closingBlock: number = positionBlock.closingBlock.toNumber()

    // true: aborted, false: overdrawn
    return blockNum > closingBlock + 200
}

/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { log } from 'src/utils/log'
import { ChromeStorageKey } from '../types/base'

const BlockNumAlarmName = 'getBlockNumber'

export async function initGetBlockNumber() {
    // init blockNum
    await getBlockNumber()

    chrome.alarms.create(BlockNumAlarmName, {
        periodInMinutes: 0.17 // 10s, 1/6 minutes
    })

    chrome.alarms.onAlarm.addListener(alarm => {
        if (alarm.name === BlockNumAlarmName) {
            void getBlockNumber()
        }
    })
}

export async function getBlockNumber() {
    try {
        const blockNum = (await globalThis.Thigh.ethersUtils.getCurrentBlockNumber()) as string
        await updateBlockNum(blockNum)
    } catch (e) {
        log(e, 'get block num error:')
        await updateBlockNum('')
    }
}

export async function updateBlockNum(blockNum: string) {
    await chrome.storage.local.set({ [ChromeStorageKey.BlockNum]: blockNum })
    await chrome.storage.local.set({ [ChromeStorageKey.OnlineStatus]: !!blockNum })
}

/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { NetworkType, NetworkTypeItem } from '../types/base'

function getTokenListUrl(net: NetworkTypeItem): string {
    let netStr: string

    if (net === NetworkType.Mainnet) {
        netStr = 'mainnet'
    }
    if (net === NetworkType.Testnet_Goerli) {
        netStr = 'goerli'
    }

    return `https://cdn.shorter.finance/tokens/${netStr}/tokenlist.json`
}

export async function getTokenList(net: NetworkTypeItem) {
    const url = getTokenListUrl(net)
    const res = await fetch(url)
    const tokenList = await (res.json() as Promise<Record<string, any>>)

    return tokenList.tokens || []
}

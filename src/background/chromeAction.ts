/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { refreshNet } from './networkAction'
import { ChromeMsgType } from '../types/base'
import { setChromeBadge } from '../utils/chromeHelper'

export function listenChromeMsg() {
    chrome.runtime.onMessage.addListener(msg => {
        if (msg === ChromeMsgType.Refresh) {
            void refreshNet()
        }
    })
}

export async function setBadgeError() {
    const errorBadge = '...'
    await setChromeBadge(errorBadge)
}
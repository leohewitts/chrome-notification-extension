/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Ref, ref } from 'vue'
import { useClickAway } from '@vant/use'

export function useMsgTypePopover() {
    const showTypePopover = ref(false)
    const showFilterPopover = ref(false)
    const handleTypePopClose = () => {
        showFilterPopover.value = false
    }

    const typePopover = ref()
    const typePopoverTrigger = ref()
    useClickAway([typePopover, typePopoverTrigger], () => {
        if (showTypePopover.value && !showFilterPopover.value) {
            showTypePopover.value = false
        }
    })

    const filterPopoverEl: Ref<Array<Element>> = ref([] as Array<Element>)
    const setFilterPopoverEl = (el: Element) => {
        filterPopoverEl.value.push(el)
    }
    useClickAway(filterPopoverEl.value, () => {
        if (showTypePopover.value && showFilterPopover.value) {
            showFilterPopover.value = false
        }
    })

    return {
        showTypePopover,
        showFilterPopover,

        typePopover,
        typePopoverTrigger,

        handleTypePopClose,
        setFilterPopoverEl
    }
}

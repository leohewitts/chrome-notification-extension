/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { ref } from 'vue'

export const Base = 'Base Events' as const
export const Other = 'Other Events' as const
export type EventCate = 'Base Events' | 'Other Events'

export function useBaseOtherEvent() {
    const showEventPopup = ref(false)
    const selectEventCate = ref<EventCate>(Base)

    const handleShowEventPopup = () => {
        showEventPopup.value = true
    }
    const handleSelectEventCate = (cate: EventCate) => {
        selectEventCate.value = cate
    }

    return {
        showEventPopup,
        selectEventCate,
        handleShowEventPopup,
        handleSelectEventCate
    }
}

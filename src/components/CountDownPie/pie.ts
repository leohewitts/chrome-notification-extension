/* eslint-disable */
/* eslint-disable @typescript-eslint/restrict-plus-operands */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
const pie = {
    options: {
        fill: ['#ff9900', '#fff4dd', '#ffc66e'],
        radius: 8
    },

    draw(opts) {
        if (!opts.delimiter) {
            const delimiter = this.raw.match(/[^0-9\.]/)
            opts.delimiter = delimiter ? delimiter[0] : ','
        }

        let values = this.values().map(n => (n > 0 ? n : 0))

        if (opts.delimiter === '/') {
            const v1 = values[0]
            const v2 = values[1]
            values = [v1, Math.max(0, v2 - v1)]
        }

        let i = 0
        let sum = 0
        let length = values.length

        for (; i < length; i++) {
            sum += values[i]
        }

        if (!sum) {
            length = 2
            sum = 1
            values = [0, 1]
        }

        const diameter = opts.radius * 2

        const $svg = this.prepare(opts.width || diameter, opts.height || diameter)

        const rect = $svg.getBoundingClientRect()
        const width = rect.width
        const height = rect.height
        const cx = width / 2
        const cy = height / 2

        const radius = Math.min(cx, cy)
        const innerRadius = opts.innerRadius

        const pi = Math.PI
        const fill = this.fill()

        const scale = (this.scale = (value: number, radius: number) => {
            const radians = (value / sum) * pi * 2 - pi / 2

            return [radius * Math.cos(radians) + cx, radius * Math.sin(radians) + cy]
        })

        let cumulative = 0

        for (i = 0; i < length; i++) {
            const value = values[i]
            const portion = value / sum
            let $node

            if (portion === 0) continue

            if (portion === 1) {
                if (innerRadius) {
                    const x2 = cx - 0.01
                    const y1 = cy - radius
                    const y2 = cy - innerRadius

                    $node = this.svgElement('path', {
                        d: [
                            'M',
                            cx,
                            y1,
                            'A',
                            radius,
                            radius,
                            0,
                            1,
                            1,
                            x2,
                            y1,
                            'L',
                            x2,
                            y2,
                            'A',
                            innerRadius,
                            innerRadius,
                            0,
                            1,
                            0,
                            cx,
                            y2
                        ].join(' ')
                    })
                } else {
                    $node = this.svgElement('circle', {
                        cx: cx,
                        cy: cy,
                        r: radius
                    })
                }
            } else {
                const cumulativePlusValue = cumulative + value

                let d = ['M'].concat(
                       // @ts-ignore
                    scale(cumulative, radius),
                    'A',
                    radius,
                    radius,
                    0,
                    portion > 0.5 ? 1 : 0,
                    1,
                    scale(cumulativePlusValue, radius),
                    'L'
                )

                if (innerRadius) {
                    d = d.concat(
                           // @ts-ignore
                        scale(cumulativePlusValue, innerRadius),
                        'A',
                        innerRadius,
                        innerRadius,
                        0,
                        portion > 0.5 ? 1 : 0,
                        0,
                        scale(cumulative, innerRadius)
                    )
                } else {
                    d.push(cx as any, cy as any)
                }

                cumulative += value

                $node = this.svgElement('path', {
                    d: d.join(' ')
                })
            }

            $node.setAttribute('fill', fill.call(this, value, i, values))

            $svg.appendChild($node)
        }
    }
}

export default pie
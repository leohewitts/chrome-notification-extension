/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import pie from './pie'

const svgElement = (tag: string, attrs: { [x: string]: any; class?: string }) => {
    const e = document.createElementNS('http://www.w3.org/2000/svg', tag)
    for (const key in attrs) {
        e.setAttribute(key, attrs[key])
    }
    return e
}

class Peity {
    $el: any
    type: any
    raw: any
    options: any
    static defaults: any
    $svg: any
    static graphers: any
    static register: (type: any, factory: any) => void

    constructor($el: any, type: any, data: any, options: any) {
        this.$el = $el
        this.type = type
        this.raw = data
        this.options = Object.assign({}, Peity.defaults[this.type], options)
    }

    svgElement(...reset: any[]) {
        // @ts-ignore
        return svgElement(...reset)
    }

    prepare(width: any, height: any) {
        if (!this.$svg) {
            this.$el.style.display = 'none'
            this.$svg = svgElement('svg', {
                class: 'peity'
            })
            this.$el.parentNode.insertBefore(this.$svg, this.$el)
        }
        this.$svg.innerHTML = ''
        this.$svg.setAttribute('width', width)
        this.$svg.setAttribute('height', height)
        return this.$svg
    }

    fill() {
        const f = this.options.fill
        const myfn = function (_: any, i: number) {
            return f[i % f.length]
        }

        return typeof f === 'function' ? f : myfn
    }

    draw() {
        Peity.graphers[this.type].call(this, this.options)
    }

    values() {
        return this.raw.split(this.options.delimiter).map((val: string) => parseFloat(val))
    }
}

Peity.defaults = {}
Peity.graphers = {}

Peity.register = (type, factory) => {
    Peity.defaults[type] = factory.options
    Peity.graphers[type] = factory.draw
}

Peity.register('pie', pie)

export default Peity

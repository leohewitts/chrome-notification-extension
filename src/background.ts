/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import Thigh from 'thigh-test-js/dist/index.es'

import { listenEventMsg } from './background/msgEventListener'
import { toggleMsgType, updateLastUpdateTime, updateMsgNum } from './background/msgAction'
import { handleSettingChange, initDefaultSetting } from './background/settingAction'
import { getNetParams, initDefaultNetType, refreshNet } from './background/networkAction'
import { listenChromeMsg, setBadgeError } from './background/chromeAction'

import { ChromeStorageKey } from './types/base'
import { initGetBlockNumber } from './background/blockNumAction'
import { log } from './utils/log'

async function init() {
    try {
        await initDefaultSetting()
        await initDefaultNetType()

        await initTigher()
        await initGetBlockNumber()
        await updateMsgNum()

        listenChromeMsg()
        await listenEventMsg()
        listenStorageChange()
    } catch (error) {
        log({ data: error, desc: 'init error' })
        await setBadgeError()
    }
}

async function initTigher() {
    try {
        const params = await getNetParams()
        log({ data: params, desc: 'init tigher with params' })

        Thigh.config.init(params)
        globalThis.Thigh = Thigh
        await updateLastUpdateTime()
    } catch (error) {
        log({ data: error, desc: 'init tigher error' })
        await setBadgeError()
    }
}

export function listenStorageChange() {
    chrome.storage.onChanged.addListener((change: Record<string, any>) => {

        if (change[ChromeStorageKey.Msgs]) {
            void updateMsgNum()
        }

        const networkChange = change[ChromeStorageKey.NetworkType]
        if (networkChange) {
            log({ data: change, desc: 'storage change' })
            void refreshNet()
            void toggleMsgType(networkChange.newValue, networkChange.oldValue)
        }

        const settingChange = change[ChromeStorageKey.Settings]
        if (settingChange) {
            log({ data: change, desc: 'storage change' })
            void handleSettingChange(settingChange)
        }
    })
}

void init()

/// <reference types="vite/client" />

declare module '*.vue' {
    import { DefineComponent } from 'vue'
    // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
    const component: DefineComponent<{}, {}, any>
    export default component
}

type ValueOf<T> = T[keyof T]

declare interface Window {
    Thigh: any
    openLog: boolean
}

declare module '*.svg' {
    const content: any
    export default content
}

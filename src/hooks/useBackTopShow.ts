import { ref } from 'vue'
import { useEventListener, useScrollParent } from '@vant/use'

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function useBackTopShow(showScrollOffset: number) {
    const topAnchorEl = ref()
    const showBackTop = ref(false)

    const scrollParent = useScrollParent(topAnchorEl)

    useEventListener(
        'scroll',
        () => {
            const scrollTop: number = (scrollParent.value as any).scrollTop as number
            showBackTop.value = scrollTop > showScrollOffset
        },
        { target: scrollParent }
    )

    return {
        topAnchorEl,
        showBackTop
    }
}

import { onMounted, onUnmounted, ref, Ref } from 'vue'
import { Position, addEvent, removeEvent, getPosition, getTransformStyle } from '../utils/dragUtils'

const eventFor = {
    mousedown: 'mousedown',
    mouseup: 'mouseup',
    mousemove: 'mousemove'
}

if ('ontouchstart' in window) {
    eventFor.mousedown = 'touchstart'
    eventFor.mouseup = 'touchend'
    eventFor.mousemove = 'touchmove'
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function useDragMove(dragEl: Ref<HTMLElement>, updatePositionCb: (p: Position) => void) {
    let shiftX = 0
    let shiftY = 0
    let moveElTranslateX = 0
    let moveElTranslateY = 0

    const xLimit = { min: -312, max: 12 }
    const yLimit = { min: -465, max: 60 }

    const isDragging = ref(false)

    function mouseMove(e: MouseEvent | TouchEvent) {
        e.stopPropagation()
        e.preventDefault()
        const position = getPosition(e)

        const x = position.x - shiftX + moveElTranslateX
        const y = position.y - shiftY + moveElTranslateY

        updateDragElPosition({ x, y })
        updatePositionCb({ x, y })

        isDragging.value = true
    }

    function mouseDown(e: MouseEvent | TouchEvent) {
        const transform = getTransformStyle(dragEl.value)
        const { x, y } = getPosition(e)
        shiftX = x
        shiftY = y

        if (transform) {
            moveElTranslateX = transform.translateX
            moveElTranslateY = transform.translateY
        }

        addEvent(document, eventFor.mousemove, mouseMove)
    }

    function mouseUp(e: MouseEvent) {
        e.stopPropagation()
        e.preventDefault()
        removeEvent(document, eventFor.mousemove, mouseMove)

        setTimeout(() => {
            isDragging.value = false
        }, 150)
    }
    
    function initDrag() {
        if (!dragEl.value) return

        addEvent(dragEl.value, eventFor.mousedown, mouseDown)
        addEvent(document, eventFor.mouseup, mouseUp)
    }

    function destroy() {
        shiftX = shiftY = moveElTranslateX = moveElTranslateY = 0

        removeEvent(dragEl.value, eventFor.mousedown, mouseDown)
        removeEvent(dragEl.value, eventFor.mouseup, mouseUp)
    }

    function updateDragElPosition(p: Position) {
        console.log('update',  p)
        let { x, y } = p
        console.log(x, xLimit.min, xLimit.max)
        console.log(y, yLimit.min, yLimit.max)

        const isXInLimit = x > xLimit.min && x < xLimit.max
        const isYInLimit = y > yLimit.min && y < yLimit.max

        if (!isXInLimit) {
            x = x < xLimit.min ? xLimit.min : xLimit.max
        }

        if (!isYInLimit) {
            y = y < yLimit.min ? yLimit.min : yLimit.max
        }

        dragEl.value.style.transform = `translate(${x}px, ${y}px)`
        console.log(dragEl.value.style)
    }

    onMounted(initDrag)
    onUnmounted(destroy)

    return {
        updateDragElPosition,
        isDragging
    }
}

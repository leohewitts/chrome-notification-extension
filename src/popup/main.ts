import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './Popup.vue'

import { Checkbox, Popover, Popup, Divider, Button, RadioGroup, Radio, Switch, Toast } from 'vant'

const app = createApp(App)
app.use(createPinia())

app.use(Checkbox)
app.use(Radio)
app.use(RadioGroup)
app.use(Popover)
app.use(Popup)
app.use(Divider)
app.use(Button)
app.use(Switch)
app.use(Toast)

app.mount('#app')

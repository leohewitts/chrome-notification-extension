/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { ref } from 'vue'
import { defineStore } from 'pinia'
import { ChromeStorageKey } from 'src/types/base'

export const useLastUpdateTimeStore = defineStore('lastUpdateTimeStore', () => {
    const lastUpdateTime = ref<number>(null)

    const initLastUpdateTime = async () => {
        chrome.storage.onChanged.addListener((change: Record<string, any>) => {
            if (change[ChromeStorageKey.LastUpdateTime]) {
                lastUpdateTime.value = change[ChromeStorageKey.LastUpdateTime]?.newValue || ''
            }
        })

        const lastUpdateTimeStorage = await chrome.storage.local.get(ChromeStorageKey.LastUpdateTime)
        lastUpdateTime.value = lastUpdateTimeStorage[ChromeStorageKey.LastUpdateTime] || lastUpdateTime.value
    }

    return {
        lastUpdateTime,
        initLastUpdateTime
    }
})

/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { computed, ref, watch } from 'vue'
import { defineStore, storeToRefs } from 'pinia'
import { useSettingStore } from './setting'
import { ChromeStorageKey, Message, MsgType } from '../types/base'
import { getMsgsFromStorage, setMsgsToStorage } from 'src/background/msgAction'

export const useMessageStore = defineStore('messageStore', () => {
    const msgs = ref([])
    const msgNum = computed(() => msgs.value.length)
    const allMsgChecked = computed(() => msgNum.value && msgs.value.every(msg => msg.isChecked))
    const noMsgChecked = computed(() => !msgNum.value || msgs.value.every(msg => !msg.isChecked))

    const initMsg = async () => {
        chrome.storage.onChanged.addListener((change: Record<string, any>) => {
            if (change[ChromeStorageKey.Msgs]) {
                msgs.value = JSON.parse(change[ChromeStorageKey.Msgs].newValue || '[]')
            }
        })

        msgs.value = await getMsgsFromStorage()
    }

    watch(
        () => msgs.value,
        () => {
            void setMsgsToStorage(msgs.value)
        }
    )

    const toggleAllMsgCheck = () => {
        if (allMsgChecked.value) {
            msgs.value = msgs.value.map(m => {
                m.isChecked = false
                return m
            })
        } else {
            msgs.value = msgs.value.map(m => {
                m.isChecked = true
                return m
            })
        }
    }

    const toggleCheckMsgAction = (msg: Message) => {
        msgs.value = msgs.value.map(m => {
            if (m.id === msg.id) {
                m.isChecked = !m.isChecked
            }
            return m
        })
    }

    const deleteMsgAction = (msg: Message) => {
        msgs.value = msgs.value.filter(m => m.id !== msg.id)
    }

    const readMsgAction = (msg: Message) => {
        msgs.value = msgs.value.map(m => {
            if (m.id === msg.id) {
                m.isReaded = true
                m.readedTime = new Date().getTime()
            }
            return m
        })
    }

    const deleteAllCheck = () => {
        msgs.value = msgs.value.filter(m => !m.isChecked)
    }

    const readAllCheck = () => {
        msgs.value = msgs.value.map(m => {
            if (m.isChecked) {
                m.isReaded = true
                m.readedTime = new Date().getTime()
            }
            return m
        })
    }

    const { filterSettings } = storeToRefs(useSettingStore())
    const currentMsgs = computed(() => {
        const { msgType, filterEvents } = filterSettings.value
        let filteredMsgs = JSON.parse(JSON.stringify(msgs.value))

        if (msgType === MsgType.Unread) {
            filteredMsgs = msgs.value.filter(m => m.isReaded === false)
        } else if (msgType === MsgType.Have_Read) {
            filteredMsgs = msgs.value.filter(m => m.isReaded === true)
        }

        filteredMsgs = filteredMsgs.filter((m: Message) => filterEvents.indexOf(m.type) !== -1)
        filteredMsgs.sort((a: Message, b: Message) => b.receiveTime - a.receiveTime)

        return filteredMsgs
    })

    return {
        msgs,
        currentMsgs,
        msgNum,
        allMsgChecked,
        noMsgChecked,

        initMsg,
        toggleAllMsgCheck,
        deleteAllCheck,
        readAllCheck,
        deleteMsgAction,
        readMsgAction,
        toggleCheckMsgAction
    }
})

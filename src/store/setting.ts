/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { reactive, Ref, ref, watch } from 'vue'
import { defineStore } from 'pinia'

import { MsgType, NetworkTypeItem, EventType, ChromeStorageKey } from '../types/base'
import { Setting, DefaultSettings, FilterSetting, DefaultNetType } from '../types/settingType'
import { getSettingFromStorage, setSettingToStorage } from '../background/settingAction'

export const useSettingStore = defineStore('settingStore', () => {
    const networkType: Ref<NetworkTypeItem> = ref(DefaultNetType)

    const settings = ref<Setting>(DefaultSettings)

    const filterSettings = reactive<FilterSetting>({
        msgType: MsgType.ALL,
        filterEvents: [...Object.values(EventType)]
    })

    watch(networkType, value => {
        void chrome.storage.local.set({ [ChromeStorageKey.NetworkType]: value })
    })

    watch(settings, value => {
        void setSettingToStorage(value)
    })

    const initNetType = async () => {
        const networkStorage = await chrome.storage.local.get(ChromeStorageKey.NetworkType)
        const networkTypeVal = networkStorage[ChromeStorageKey.NetworkType]

        networkType.value = networkTypeVal || DefaultNetType
    }

    const initSetting = async () => {
        const settingsVal = await getSettingFromStorage()

        settings.value = settingsVal || DefaultSettings
    }

    const updateSettingsAction = (newSettings: Setting) => {
        settings.value = newSettings
        void setSettingToStorage(newSettings)
    }

    return {
        networkType,
        initNetType,
        settings,
        initSetting,
        filterSettings,
        updateSettingsAction
    }
})

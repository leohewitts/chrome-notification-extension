/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { ref } from 'vue'
import { defineStore } from 'pinia'
import { ChromeStorageKey } from 'src/types/base'

export const useBlockNumStore = defineStore('blockNumStore', () => {
    const blockNum = ref<number | string>(null)

    // get blockNum successful, it means that it is currently online
    const onlineStatus = ref(false)

    const initBlockNum = async () => {
        chrome.storage.onChanged.addListener((change: Record<string, any>) => {
            if (change[ChromeStorageKey.BlockNum]) {
                blockNum.value = change[ChromeStorageKey.BlockNum]?.newValue || ''
            }
        })

        const blockStorage = await chrome.storage.local.get(ChromeStorageKey.BlockNum)
        blockNum.value = blockStorage[ChromeStorageKey.BlockNum] || blockNum.value
    }

    const initOnlineStatus = async () => {
        chrome.storage.onChanged.addListener((change: Record<string, any>) => {
            if (change[ChromeStorageKey.OnlineStatus]) {
                onlineStatus.value = change[ChromeStorageKey.OnlineStatus]?.newValue
            }
        })

        const onlineStatusStorage = await chrome.storage.local.get(ChromeStorageKey.OnlineStatus)
        onlineStatus.value = onlineStatusStorage[ChromeStorageKey.OnlineStatus]
    }

    return {
        blockNum,
        onlineStatus,

        initBlockNum,
        initOnlineStatus
    }
})

import { EventItem, EventType, MsgTypeItem, NetworkType, NetworkTypeItem } from './base'

export interface Setting {
    rpc: RpcConfig
    events: Array<EventItem>
    biddingTantoOpen: boolean
    biddingTantos: BdTantoObj[]
    removeAfterTime: number
    soundOpen: boolean
    version?: string
}

export interface BdTantoObj {
    key: string | number
    value: string
    validatorStatus: 'patternError' | 'repeatError' | null
}

export type RpcConfig = {
    [K in NetworkTypeItem]: string
}

export interface FilterSetting {
    msgType: MsgTypeItem
    filterEvents: Array<EventItem>
}

export const DefaultSettings = {
    rpc: {
        [NetworkType.Mainnet]: process.env.VUE_APP_RPC_MAINNET_URL,
        [NetworkType.Testnet_Goerli]: process.env.VUE_APP_RPC_TESTNET_GOERLI_URL
    },
    events: [...Object.values(EventType).filter(e => e !== EventType.Tanto_Bidding)],
    biddingTantoOpen: false,
    biddingTantos: [],
    removeAfterTime: 300,
    soundOpen: false
}

export const DefaultNetType = NetworkType.Mainnet

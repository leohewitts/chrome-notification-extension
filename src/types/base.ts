export const NetworkType = {
    Mainnet: 'Mainnet',
    Testnet_Goerli: 'Testnet'
} as const
export type NetworkTypeItem = ValueOf<typeof NetworkType>

export const MsgType = {
    ALL: 'All',
    Unread: 'Unread',
    Have_Read: 'Have read',
    Filter: 'Filter'
} as const
export type MsgTypeItem = ValueOf<typeof MsgType>

export const EventType = {
    Position_Opened: 'Position Opened',
    New_Proposal: 'New Proposal',
    Position_Overdrawn: 'Position Overdrawn',
    Liquidation_Aborted: 'Liquidation Aborted',
    New_Liquidation: 'New Liquidation',
    Proposal_Passed: 'Proposal Passed',
    Proposal_Failed: 'Proposal Failed',
    Tanto_Bidding: 'Tanto Bidding'
} as const
export type EventItem = ValueOf<typeof EventType>

export const ChromeStorageKey = {
    OnlineStatus: 'onlineStatus',
    Msgs: 'msgs',
    Settings: 'settings',
    NetworkType: 'networkType',
    BlockNum: 'blockNum',
    BackTopPosition: 'backTopPosition',
    InActiveMsgs: 'inActiveMsgs',
    LastUpdateTime: 'lastUpdateTime'
} as const

export const ChromeMsgType = {
    Refresh: 'refresh'
}

export interface MsgLog {
    log: Record<string, any>
    origin: Record<string, any>
}

export interface Message {
    id: number
    name: string
    eventName: string

    hash: string
    proposalId: string
    transactionHash: string
    positionAddr: string

    poolId: string
    logoURI: string

    type: EventItem
    receiveTime: number
    isChecked: boolean
    isReaded: boolean
    readedTime: number

    hashStr: string
    receiveDateStr: string
    receiveTimeStr: string

    args: Record<string | number, any>
    log: Record<string | number, any>
    origin: Record<string | number, any>
}

export const ShowPositionHashArr = new Set([
    EventType.Position_Opened,
    EventType.Position_Overdrawn,
    EventType.New_Liquidation,
    EventType.Liquidation_Aborted
])
export const ShowCreateByArr = new Set([EventType.New_Proposal, EventType.Proposal_Passed, EventType.Proposal_Failed])
export const ShowBidderArr = new Set([EventType.Tanto_Bidding])

export const OpenBlockExplore = new Set([EventType.Position_Opened])
export const OpenShorterLiquidationPage = new Set([
    EventType.Position_Overdrawn,
    EventType.New_Liquidation,
    EventType.Liquidation_Aborted,
    EventType.Tanto_Bidding
])
export const OpenShorterProposalPage = new Set([EventType.Proposal_Passed, EventType.Proposal_Failed, EventType.New_Proposal])

export const ProposalStatusMap = {
    Passed: 1,
    Failed: 2
}

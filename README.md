Shorter Alarm Chrome Extension

## Build
`npm run build`

## Hot reload
`npm run dev`

> Use Chrome to directly load the dist folder as an extension (need to open extension developer mode)
module.exports = {
    root: true,
    env: {
        node: true,
        browser: true
    },
    extends: [
        'plugin:vue/vue3-recommended',
        'plugin:vue/vue3-essential',
        '@vue/typescript/recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'prettier'
    ],
    parser: 'vue-eslint-parser',
    parserOptions: {
        sourceType: 'module',
        ecmaVersion: 2020,
        tsconfigRootDir: __dirname,
        project: ['./tsconfig.eslint.json'],
        parser: '@typescript-eslint/parser'
    },
    plugins: ['vue', 'html', '@typescript-eslint'],
    rules: {
        indent: ['error', 4, { SwitchCase: 1 }],
        semi: ['error', 'never', { beforeStatementContinuationChars: 'never' }],
        'semi-spacing': ['error', { after: true, before: false }],
        'semi-style': ['error', 'first'],
        'no-extra-semi': 'error',
        'no-unexpected-multiline': 'error',
        'no-unreachable': 'error',
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        // vue
        'vue/no-deprecated-slot-attribute': 2,
        'vue/no-deprecated-scope-attribute': 2,
        'vue/no-deprecated-slot-scope-attribute': 2,
        'vue/no-deprecated-filter': 2,
        'vue/no-deprecated-v-bind-sync': 2,
        'vue/no-deprecated-v-on-number-modifiers': 2,
        'vue/no-deprecated-events-api': 2,
        'vue/no-deprecated-functional-template': 2,
        'vue/no-deprecated-html-element-is': 2,
        'vue/no-deprecated-vue-config-keycodes': 2,
        'vue/no-deprecated-dollar-listeners-api': 2,
        'vue/no-deprecated-v-on-native-modifier': 2,
        'vue/no-deprecated-dollar-scopedslots-api': 2,

        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-unsafe-member-access': 'off',
        '@typescript-eslint/restrict-template-expressions': 'off'
    }
}
